# NOTE: alpine-node:10 rise errors when installing thrift.

FROM alpine:edge
LABEL Nosebit Dev Team <dev@nosebit.com>

# Environment variables
ENV NODE_VERSION=8.9.4

# Install dev dependencies
RUN run_pkgs="build-base make bash python curl tar xz linux-headers binutils-gold gnupg libstdc++" && \
    apk --update add ${run_pkgs} ${build_pkgs}

# Install dev dependencies
#RUN build_pkgs="gcc g++ python git autoconf automake boost libtool flex bison" && \
#    run_pkgs="make bash" && \
#   apk --update add ${run_pkgs} ${build_pkgs}

# Install nodejs 8
RUN cd /tmp && \
    curl -sO https://nodejs.org/dist/v${NODE_VERSION}/node-v${NODE_VERSION}.tar.xz && \
    tar -xf node-v${NODE_VERSION}.tar.xz && \
    cd node-v${NODE_VERSION} && \
    ./configure --prefix=/usr --fully-static && \
    make -j$(getconf _NPROCESSORS_ONLN) && \
    make install && \
    rm -rf /tmp/node-v${NODE_VERSION} /tmp/node-v${NODE_VERSION}.tar.xz

# Remove build packages
RUN apk del ${build_pkgs}
